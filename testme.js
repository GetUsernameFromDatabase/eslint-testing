"use strict";
// OIOIOIOI
var a = 1234;
var b = "testme";

console.log("b", b);

// var camelCase = 1234;
// var CAMEL_CASE = 1234;
// var SNAKE_CASE = 1234;

var foo = {
  bar: "This is a bar.",
  baz: { qux: "This is a qux" },
  difficult: "to read",
};
console.log("foo", foo);

function kassOliPuuOtsas(mituKassi) {
  if (mituKassi > 1) {
    console.log("aita kassid alla!");
  } else {
    console.log("aita kass alla!");
  }
}

kassOliPuuOtsas(1);
kassOliPuuOtsas(2);

console.log("test me!!!");

var data = {
  rules: {
    semi: ["error", "always"],
    quotes: ["error", "double"],
  },
};

if (a == 1234) {
  console.log("a on 1234");
}

console.log("data", data);

// // console.log(1+2);
// var code = 'console.log(1+2+3);';
// console.log('eval goes here:');
// eval(code);
